import jwt from "jsonwebtoken";
const secret = 'test';

export const verifyUser = async (req,res,next)=>{
    try{
        
    const token = req.headers.authorization.split(" ")[1];
    
    
    let decodedData;
    if(token){
        decodedData = jwt.verify(token, secret);
        req.userId = decodedData.id;
        
    }
    next();
    }catch(error){
      console.log(error);
    }
}

export const verifyAdmin = async (req,res,next)=>{
    try{
        
    const token = req.headers.authorization.split(" ")[1];
    
    
    let decodedData;
    if(token){
        decodedData = jwt.verify(token, secret);
        req.userAdmin = decodedData.admin;
        
        if(req.userAdmin){
            next();
        }else{
            res.status(403).json({message:'You are not authorized to perform this operation!'})
        }
    }
    
    
    }catch(error){
      console.log(error);
    }
}

