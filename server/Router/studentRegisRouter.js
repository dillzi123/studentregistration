import express from 'express';
import {getRegStud, signin,regisStudent, updateRegis, deleteRegis} from '../controller/studentRegisController.js';
import {verifyUser,verifyAdmin} from '../Middleware/auth.js';

const regisStudRoutes = express.Router();

regisStudRoutes.get('/',verifyUser,getRegStud);
regisStudRoutes.post('/signin',signin);
regisStudRoutes.post('/',regisStudent);
regisStudRoutes.patch('/:id',verifyUser,updateRegis);
regisStudRoutes.patch('/:id',verifyAdmin,updateRegis);
regisStudRoutes.delete('/:id',verifyUser,deleteRegis);
regisStudRoutes.delete('/:id',verifyAdmin,deleteRegis);



export default regisStudRoutes;