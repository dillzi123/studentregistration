import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import mongoose from 'mongoose';
//import postRoutes from './Routers/postRouter.js';
import regisStudRoutes from './Router/studentRegisRouter.js';
import bonafiedStudRoutes from './Router/bonaStudRouter.js';
const CONNECTION_URL = 'mongodb://localhost:27017/regStudentApp';
//const connect = mongoose.connect(url,{useNewUrlParser:true, useUnifiedTopology:true});

const app = express();


app.use(bodyParser.json({limit:"30mb",extended:true }));
app.use(bodyParser.urlencoded({limit:"30mb",extended:true}));
app.use(cors());
app.use('/registration',regisStudRoutes);
app.use('/students',bonafiedStudRoutes);
//const CONNECTION_URL = 'mongodb+srv://memoApp:memoApp123@memocluster.xlclh.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
const PORT = process.env.PORT||5000;

mongoose.connect(CONNECTION_URL,{useNewUrlParser:true, useUnifiedTopology:true})
.then(()=>{
app.listen(PORT,()=>{
    console.log(`Server running on port: ${PORT}`);})
})
.catch((err)=>{
console.log(err.message)
});



mongoose.set('useFindAndModify', false);
