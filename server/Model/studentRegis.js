import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const studeRegisSchema = new Schema({
    firstName:{type:String, required:true},
    lastName:{type:String, required:true},
    email:{type:String, required:true},
    admin:{type:Boolean, default:false},
    enrollment_num:{type:String, required:true},
    faculty_num:{type:String, required:true},
    selectedFile:String,
    department:{type:String, required:true},
    subjects:{type:[String], default:[]},
    password:{type:String, required:true},
    confirmPassword:{type:String, required:true}
    

},{timestamps:true});

export default mongoose.model('RegisStudent',studeRegisSchema);
