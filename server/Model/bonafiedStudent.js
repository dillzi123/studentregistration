import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const bonafiedStudentSchema = new Schema({

    
    enrollment_num:{type:String, required:true},
    faculty_num:{type:String, required:true},
    email:{type:String, required:true},
    
},{timestamps:true});

export default mongoose.model('BonaStudent',bonafiedStudentSchema);
