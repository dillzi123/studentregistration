import RegisStudentModel from '../Model/studentRegis.js';
import BonaStudent from '../Model/bonafiedStudent.js';
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import express from 'express';
import mongoose from 'mongoose';
const secret = 'test'




export const getRegStud =async (req,res)=>{
  try{
    const data = await RegisStudentModel.find();
    res.status(200).json(data);
  }catch(error){
     res.status(404).json({message:error.message});
  }   
}

export const signin = async (req, res) => {
    const data = req.body;
  
    try {
      const oldUser = await RegisStudentModel.findOne({enrollment_num:data.enrollment_num});
  
      if (!oldUser) return res.status(404).json({ message: "User doesn't exist" });
  
      const isPasswordCorrect = await bcrypt.compare(data.password, oldUser.password);
  
      if (!isPasswordCorrect) return res.status(400).json({ message: "Invalid credentials" });
  
      const token = jwt.sign({ email: oldUser.email, id: oldUser._id, admin: oldUser.admin}, secret, { expiresIn: "1h" });
  
      res.status(200).json({ result: oldUser, token });
    } catch (err) {
      res.status(500).json({ message: "Something went wrong" });
    }
  };
  

export const regisStudent = async (req,res)=>{
    const data = req.body;
   // const newPost = new RegisStudentModel(data);
    try{
        const checkBonafied = await BonaStudent.findOne({enrollment_num:data.enrollment_num})
        if(checkBonafied){
           const checkRegis = await RegisStudentModel.findOne({enrollment_num:data.enrollment_num});
            if(!checkRegis){
                if(data.password !== data.confirmPassword) return res.status(400).json({ message: "Password doesn't matching" });
   
                const hashedPassword = await  bcrypt.hash(data.password, 12)
                data.password = hashedPassword;
                data.confirmPassword = hashedPassword;
                const newdata= await RegisStudentModel.create(data);
                const token = jwt.sign( { email: newdata.email, id: newdata._id, admin: newdata.admin }, secret, { expiresIn: "1h" } );

                if(newdata){
                res.status(201).json({ result: newdata, token });
                }else{
                res.status(400).json(` Registration Failed`);
                }
            }else{
               res.status(400).json(`registration is already done`);
            }
        }else{
            res.status(404).json(`Not a bonafied student`);
        }
    }catch(error){
        res.status(409).json({message:error.message});
    }
}

export const updateRegis = async (req, res) => {
  const  id  = req.params.id;
  const data = req.body;
 // if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`No post with id: ${id}`);
 try{

  const updatedRegis = await RegisStudentModel.findByIdAndUpdate(id, data, { new: true });
  if(updatedRegis){
    res.status(201).json(updatedRegis);
  }else{
    res.status(400).json(`Could not be updated`);
    
  }
  
}catch(error){
    res.status(409).json({message:error.message});
}
}


  export const deleteRegis = async (req,res) =>{
    const id = req.params.id;
    try{
    const data =  await RegisStudentModel.findByIdAndDelete(id);
    if(data){
      res.status(200).json(`Deleted registered student with id:${id}`);
    }else{
      res.status(404).json(` Registered student with id:${id} does not exist`);
    }
     
    }catch(err){
      res.status(409).json({message:error.message}); 
    }
  }

  