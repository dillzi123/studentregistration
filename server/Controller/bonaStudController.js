import BonaStudent from '../Model/bonafiedStudent.js';

export const bonafiedStudent = async (req, res) => {
    const data= req.body;
  
    try {
      const oldUser = await BonaStudent.findOne({enrollment_num:data.enrollment_num});
  
      if (oldUser) return res.status(400).json({ message: "Student already exists" });
      
      const result = await BonaStudent.create(data);
      res.status(201).json({ result});
    } catch (error) {
      res.status(500).json({ message: "Something went wrong" });
      
      console.log(error);
    }
  };