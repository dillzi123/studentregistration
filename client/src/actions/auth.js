import * as api from '../api/index';

export const signin =  (formData, history)=> async (dispatch)=>{
    try{
       const {data} = await api.signin(formData);
       dispatch({type:'AUTH',data});
       history.push('./')
    }catch(error){
        console.log(error);
    }
 }
 
 export const registration =  (formData, history)=> async (dispatch)=>{
    try{
      const {data} = await api.signup(formData);
      dispatch({type:'AUTH',data});
      history.push('./')
    }catch(error){
      console.log(error);
    }
 }

 export const updateRegis =  (id, formData)=> async (dispatch)=>{
   try{
     const {data} = await api.updateRegis(id,formData);
     dispatch({type:'UPDATE',payload:data})
   }catch(error){
       console.log(error);
   }
}

export const getRegis = ()=> async (dispatch)=>{
  try{
    const {data} = await api.getRegis();
    dispatch({type:'GET',payload:data});
  }catch(error){
    console.log(error);
  }
}

export const deleteRegis = (id)=> async (dispatch)=>{
  try{
    await api.deleteRegis(id);
    dispatch({type:'DELETE',payload:id})
   }catch(error){
       console.log(error);
   }
}