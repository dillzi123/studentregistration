import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import {Container} from '@material-ui/core';
import Navbar from './Components/Navbar/navbar';
import Home from './Components/Home/home';
import Auth from './Components/Auth/Auth';

 
const App = ()=>{

    return(
    <BrowserRouter>
       <Navbar />
        <Container>
            <Switch>
                <Route path='/' exact component={Home} />
                <Route path='/auth' exact component={Auth} />
                
            </Switch>
        </Container>
    </BrowserRouter>
     
        );
}


export default App;