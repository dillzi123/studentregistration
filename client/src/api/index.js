import axios from 'axios';


const API = axios.create({baseURL: "http://localhost:5000"});

API.interceptors.request.use((req)=>{
    if(localStorage.getItem('profile')){
        req.headers.Authorization = `Bearer ${JSON.parse(localStorage.getItem('profile')).token}`;
    }
    return req;
});

export const getRegis = ()=> API.get('./registration');
export const signin = (postData)=>API.post('./registration/signin',postData);
 export const signup = (postData)=>API.post('./registration',postData);
 export const updateRegis = (id,updatedPost)=>API.patch(`./registration/${id}`,updatedPost);
 export const deleteRegis = (id)=>API.delete(`./registration/${id}`);

