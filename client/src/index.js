import React from 'react';
import ReactDom from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import Apps from './App';
import {reducers} from './reducers';
import './index.css'; 
import 'bootstrap/dist/css/bootstrap.min.css';

const store = createStore(reducers, compose(applyMiddleware(thunk)));

ReactDom.render(
<Provider store={store}>
< Apps />
</Provider>,document.getElementById('root'));

