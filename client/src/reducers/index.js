import {combineReducers} from 'redux';
import posts from './post';
import authReducer from './auth';

export const reducers= combineReducers({ posts, authReducer});