import React, {useState, useEffect} from 'react';
import {useDispatch} from "react-redux";
import {useHistory} from 'react-router-dom';
import {Avatar, Grid, Button, Paper, Typography, Container} from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import FileBase from 'react-file-base64';
import useStyles from './styles';
import Input from './input';
import Icon from './Icon';
import {registration, signin} from '../../actions/auth';

const initialState = {firstName:'', lastName:'',enrollment_num:'',faculty_num:'', 
department:'', email:'', subjects:'', password:'', confirmPassword:'', selectedFile:''};
const Auth = () => {
    
    const classes = useStyles();
    const [showPassword, setShowPassword] = useState(false);
    const handleShowPassword = () => setShowPassword((prevShowPassword)=>!prevShowPassword);
    const [isSignup,setIsSignup] = useState(false);
    const dispatch = useDispatch(); 
    const history = useHistory();
    const [formData, setFormData] = useState(initialState);
    const handleSubmit = (e)=>{
     e.preventDefault();

    if(isSignup){
        dispatch(registration(formData, history));
    }else{
        dispatch(signin(formData, history));
    }
    }
    const handleChange = (e)=>{
     setFormData({...formData, [e.target.name]:e.target.value});
    }
    const switchMode = ()=>{
        setIsSignup((prevSign)=>!prevSign);
        setShowPassword(false);
        
    }
    

    return (
        <Container component='main' maxWidth='sm' >
            
            <Paper className={classes.paper} elevation={3} >
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography variant='h5'>{isSignup ?'Register':'Login'}</Typography>
                <form className={classes.form} onSubmit={handleSubmit}>
                    <Grid container spacing={2} >
                        {
                            isSignup && (
                                <>
                                    <Input name="firstName" label="First Name" handleChange={handleChange} 
                                    autoFocus half/>

                                    <Input name="lastName" label="Last Name" handleChange={handleChange}
                                    half/>

                                    <Input name="enrollment_num" label="Enrollment No." handleChange={handleChange}
                                    half/>

                                    <Input name="faculty_num" label="Faculty No." handleChange={handleChange}
                                    half/>
                                </>
                            )
                        }           {
                                    isSignup && (
                                    <>    
                                    <Input name="department" label="Department" handleChange={handleChange} />
                                    <Input name="subjects" label="Subjects" handleChange={handleChange} />
                                    </>
                                    )
                                    }  
                                    {
                                    isSignup ?(
                                    <Input name="email" label="Email Address" handleChange={handleChange}
                                    type='email'/>):
                                    (<Input name="enrollment_num" label="Enrollment No." handleChange={handleChange}/>)
                                    }

                                    <Input name="password" label="Password" handleChange={handleChange}
                                    type={showPassword?'text':'password'} handleShowPassword={handleShowPassword}/>

                                    {isSignup && (
                                    <>    
                                    <Input name="confirmPassword" label="Confirm Password" handleChange={handleChange}
                                    type='password'/>
                                    <div className={classes.fileInput}>
                                    <FileBase type='file' multiple={false}
                                    onDone={({base64})=> setFormData({...formData, selectedFile:base64})} />
                                    </div>
                                    </>
                                    )}
                                    </Grid>
                                    <Button type='submit' fullWidth variant='contained' color='primary' className={classes.submit}>
                                        {isSignup ? 'Register' : 'Login'}
                                    </Button>
                                    
                                    
                                    
                                    
                                    <Grid container justify='flex-end'>
                                        <Grid item>
                                            <Button onClick={switchMode}>
                                                {isSignup? 'Are you Registered student? Login':
                                                           "Do you want to do registration? Register"}
                                            </Button>
                                        </Grid>
                                    </Grid>
                                
                                
                    
                </form>
            </Paper>
            
        </Container>
    )
}

export default Auth;
