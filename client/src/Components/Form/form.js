import React,{useState,useEffect} from 'react';
import FileBase from 'react-file-base64';
import {Avatar, Grid, Button, Paper, Typography, Container, TextField} from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {useDispatch,useSelector} from 'react-redux';
import useStyles from './styles';
import Input from './input';
import {updateRegis} from '../../actions/auth';



//const initialState = {firstName:'', lastName:'',enrollment_num:'',faculty_num:'', 
//department:'', email:'', subjects:'', password:'', confirmPassword:'', selectedFile:''};

const Form = ({currentId,setCurrentId})=>{

const dispatch = useDispatch();
const [postData,setPostData] = useState( {firstName:'', lastName:'',enrollment_num:'',faculty_num:'', 
department:'', email:'', subjects:'', password:'', confirmPassword:'', selectedFile:''}
);
const [showPassword, setShowPassword] = useState(false);
const handleShowPassword = () => setShowPassword((prevShowPassword)=>!prevShowPassword);

const post = useSelector((state)=>
(currentId?state.posts.find((p)=>p._id===currentId):null));

const classes = useStyles();
const user = JSON.parse(localStorage.getItem('profile'));

useEffect(()=>{
    if(post){
    setPostData(post);
    }
},[post]);
console.log('bp',postData);
const clear = ()=>{
    setCurrentId(0);
    setPostData( {firstName:'', lastName:'',enrollment_num:'',faculty_num:'', 
    department:'', email:'', subjects:'', password:'', confirmPassword:'', selectedFile:''})
 };
//  const handleChange = (e)=>{
//     setPostData({...postData, [e.target.name]:e.target.value});
//  }


const handleSubmit = (e)=>{
 e.preventDefault();
 if(currentId){
    dispatch(updateRegis(currentId,postData));   
    clear();  
 }

     
}
 if(!user?.result?._id){
     return(
         
         <Paper className={classes.paper}>
             <Typography variant="h6" align="center">
                 Click on login button to do registeration!
             </Typography>
         </Paper>
     );
 }


    return(
        <Container component='main' maxWidth='sm' >
           
            <Paper className={classes.paper} elevation={3} >
                <Typography variant='h5'>Update Registration</Typography>
                <form autoComplete='off' noValidate   className={`${classes.root} ${classes.form}`} onSubmit={handleSubmit}>
                    <Grid container spacing={2} >
                    <Grid item xs={12} sm={6}>
                    <TextField name='firstName' variant='outlined' label='First Name' fullWidth
                value={postData.firstName} 
                onChange={(e)=>setPostData({...postData, firstName: e.target.value})}  />
                </Grid>
                    <Grid item xs={12} sm={6}>
                <TextField name='lastName' variant='outlined' label='Last Name' fullWidth
                value={postData.lastName} 
                onChange={(e)=>setPostData({...postData, lastName: e.target.value})}  />
                 </Grid>  

                 <Grid item xs={12} sm={6}>
                    <TextField name='enrollment_num' variant='outlined' label='Enrollment No.' fullWidth
                value={postData.enrollment_num} 
                onChange={(e)=>setPostData({...postData, enrollment_num: e.target.value})}  />
                </Grid>
                    <Grid item xs={12} sm={6}>
                <TextField name='faculty_num' variant='outlined' label='Faculty No.' fullWidth
                value={postData.faculty_num} 
                onChange={(e)=>setPostData({...postData, faculty_num: e.target.value})}  />
                 </Grid> 

                 <Grid item xs={12} sm={6}>
                    <TextField name='department' variant='outlined' label='Department' fullWidth
                value={postData.department} 
                onChange={(e)=>setPostData({...postData, department: e.target.value})}  />
                </Grid>
                    <Grid item xs={12} sm={6}>
                <TextField name='email' variant='outlined' label='Email Address' fullWidth
                value={postData.email} 
                onChange={(e)=>setPostData({...postData, email: e.target.value})}  />
                 </Grid> 

                 <TextField name='subjects' variant='outlined' label='Subjects' fullWidth
                value={postData.subjects} 
                onChange={(e)=>setPostData({...postData, subjects: e.target.value})}  />
                <TextField name='password' variant='outlined' label='Password' fullWidth
                value={postData.password} 
                onChange={(e)=>setPostData({...postData, password: e.target.value})}  />

                <div className={classes.fileInput}>
                    <FileBase type='file' multiple={false}
                    onDone={({base64})=> setPostData({...postData, selectedFile:base64})} />
                </div>
              </Grid>
                <Button className={classes.buttonSubmit} variant='contained' color='primary'
                 size='large' type="submit" fullWidth>Submit</Button>  
                <Button variant='contained' color='secondary'
                 size='small' onClick={clear} fullWidth>Clear</Button>     

                                    
                                    
                                    
                                    
                                
                                
                    
                </form>
            </Paper>
            
        </Container>
    );
    }

export default Form;
