import React from 'react';
import useStyles from './style';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import DeleteIcon from '@material-ui/icons/Delete';
import {Button, Typography, Card, CardContent, TextField} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import {useDispatch} from 'react-redux';
import {deleteRegis} from '../../../actions/auth';


const Post = (props)=>{
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = JSON.parse(localStorage.getItem('profile'));
    if(!user?.result?._id){
        return(
            
            <Paper className={classes.paper}>
                <Typography variant="h6" align="center">
                  Welcome to Home Page!
                </Typography>
            </Paper>
        );
    }
    return(
        <TableContainer component={Paper}>
        <Card className={classes.card}>
            
            <Typography>Registration Detail</Typography>
        <div className={classes.overlay2}>
                <Button style={{color:'blue'}} size='small' onClick={()=>props.setCurrentId(props.post._id)}>
                    <MoreHorizIcon fontSize='default'/>
                </Button>
            </div>
        
        
            <CardContent>
        <Table className={classes.table} aria-label="simple table">
        
             <TableBody>
             
                 <TableRow>
                     <TableCell component="th" scope="row">First Name</TableCell>
                     <TableCell >{props.post.firstName}</TableCell>
                     <TableCell component="th" scope="row">Last Name</TableCell>
                     <TableCell >{props.post.lastName}</TableCell>
                 </TableRow>
                 <TableRow>
                     <TableCell component="th" scope="row">Enrollment No.</TableCell>
                     <TableCell >{props.post.enrollment_num}</TableCell>
                     <TableCell component="th" scope="row">Faculty No.</TableCell>
                     <TableCell >{props.post.faculty_num}</TableCell>
                 </TableRow>
                 <TableRow>
                     <TableCell component="th" scope="row">Department</TableCell>
                     <TableCell >{props.post.department}</TableCell>
                     <TableCell component="th" scope="row">Email Address</TableCell>
                     <TableCell >{props.post.email}</TableCell>
                 </TableRow>
                 <TableRow>
                     <TableCell component="th" scope="row">Subjects</TableCell>
                     <TableCell >{props.post.subjects}</TableCell>
                 </TableRow>
                 <TableRow>
                 <TableCell component="th" scope="row">
                     <form className='d-print-none'>
                     <Button className={classes.buttonSubmit} variant='contained' color='primary'
                 size='small' type="submit" fullWidth value='Print' onClick='window.print()'>Print</Button> 
                     </form>
                     
                 </TableCell>
                 <TableCell>
                {user.result.admin?(<Button size="small" color="secondary" onClick={() => dispatch(deleteRegis(props.post._id))}>
           <DeleteIcon fontSize="small" /> Delete
           </Button> ):null}
                 
                 </TableCell>
                     
                
                     
                 </TableRow>
                 
             </TableBody>

        </Table>
        
        </CardContent>
    
    </Card>
    </TableContainer>
   
    )
}
export default Post;              

        