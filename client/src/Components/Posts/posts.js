import React from 'react';
import {Grid,CircularProgress} from '@material-ui/core';
import {useSelector} from 'react-redux';
import Post from './Post/post';
import useStyles from './style';


const Posts = (props)=>{
    const classes = useStyles();
    const user = JSON.parse(localStorage.getItem('profile'));
    const regis = useSelector((state)=>state.posts);
    
   
    return(
      !user?.result?.admin ? (
        <Grid className={classes.container} >
        
      <Post post={user?.result} setCurrentId={props.setCurrentId}/>
 
  </Grid>):(
      <Grid className={classes.container} container alignItems='stretch' spacing={3}>
              
             
                {regis.map((reg)=>(
              <Grid key={reg._id} item >
                  <Post post={reg} setCurrentId={props.setCurrentId}/>
              </Grid>
          ))}
      
             
        
      </Grid>
    )
    )
 }

export default Posts;
