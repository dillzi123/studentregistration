import React, {useState,useEffect} from 'react';
import {Link, useHistory, useLocation} from 'react-router-dom';
import {useDispatch} from 'react-redux';
import decode from 'jwt-decode';
import { AppBar, Typography, Toolbar, Avatar, Button } from '@material-ui/core';
import useStyles from './styles';
import memories from '../../images/logo.png';

const Navbar = ()=>{
    const classes = useStyles();
    const history = useHistory();
    const dispatch = useDispatch();
    const location = useLocation();
    const [user,setUser] = useState(JSON.parse(localStorage.getItem('profile')));
    
    const logOut = ()=>{
      dispatch({type:'LOGOUT'});
      history.push('/auth');
      setUser(null);
    };

   useEffect(() => {
     const token = user?.token;
     if (token) {
      const decodedToken = decode(token);

      if (decodedToken.exp * 1000 < new Date().getTime()){
         logOut();
      }
    }

     setUser(JSON.parse(localStorage.getItem('profile')));
   }, [location]);
 
 
    return(
        <AppBar className={classes.appBar} position='static' color='inherit'>
        <div className={classes.brandContainer}>
        <Typography component={Link} to='/' className={classes.heading} variant='h2' align='center'>Student Registration Portal</Typography>
        <img className={classes.image} src={memories} alt='memories' height='60' />
        </div>
        <Toolbar className={classes.toolbar}>
        {user ? (
          <div className={classes.profile}>
            <Avatar className={classes.purple} alt={user.name} src={user.result?.selectedFile}>{user.result?.firstName?.charAt(0)}</Avatar>
            <Typography className={classes.userName} variant="h6">{user.result?.firstName+" "+ user.result?.lastName}</Typography>
            <Button variant="contained" className={classes.logout} color="secondary" onClick={logOut}>Logout</Button>
          </div>
        ) : (
          <Button component={Link} to="/auth" variant="contained" color="primary">Login</Button>
        )}
      </Toolbar>
     </AppBar>
    )
}

export default Navbar; 